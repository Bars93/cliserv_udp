CMAKE = /usr/bin/cmake
DEFGEN = "Unix Makefiles"
CURDIR = $(shell pwd)
all:
	echo "Create build directory in ${CURDIR}" &&\
	mkdir -p ${CURDIR}/build &&\
	cd ${CURDIR}/build &&\
	echo "Init with ${CMAKE}" &&\
	${CMAKE} -G ${DEFGEN} .. &&\
	cd .. &&\
	echo "Ready!"
.PHONY: clean
clean:
	rm -rvf build
